package br.libutil.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import br.libutil.string.StringUtil;



public class ReplaceMsgTest {

	@Before
	public void setUp() throws Exception {
	}

	
	@Test
	public void test2param(){
		
		String text = "Msg {0} test {1} replace";
		
		String result = StringUtil.replace(text, "TEST 1", "TEST 2");
		
		assertEquals("Msg TEST 1 test TEST 2 replace", result);
		
	}
	
	@Test
	public void test1param(){
		
		String text = "Msg {0} test {1} replace";
		
		String result = StringUtil.replace(text, "TEST 1");
		
		assertEquals("Msg TEST 1 test {1} replace", result);
		
	}
	
	@Test
	public void testNoParam(){
		
		String text = "Msg {0} test {1} replace";
		
		String result = StringUtil.replace(text);
		
		assertEquals(text, result);
		
	}
	
	@Test
	public void testBlankParam(){
		
		String text = "Msg {0} test {1} replace";
		
		String result = StringUtil.replace(text, "");
		
		assertEquals("Msg  test {1} replace", result);
		
	}
	
	@Test
	public void testNullParam(){
		
		String text = "Msg {0} test {1} replace";
		
		String result = StringUtil.replace(text, null);
		
		assertEquals(text, result);
		
	}
	
	@Test
	public void testNumberParam(){
		
		String text = "Msg {0} test {1} replace";
		
		String result = StringUtil.replace(text, 1,2,3);
		
		assertEquals("Msg 1 test 2 replace", result);
	}
	
	@Test(expected = NullPointerException.class)
	public void errorText(){
		
		String text = null;
		
		String result = StringUtil.replace(text, 1,2,3);
		
		assertEquals("Msg 1 test 2 replace", result);
	}
}
