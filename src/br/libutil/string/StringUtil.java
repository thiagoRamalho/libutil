package br.libutil.string;

public class StringUtil {
	
	/**
	 * Sobreescreve a ocorrência dos parâmetros no texto enviado
	 * @param text
	 * @param parameters
	 * Exemplo: Texto {0} => Texto Parametro
	 * @return
	 */
	public static String replace(String text, Object... parameters){

		if(parameters != null && parameters.length > 0){

			for (int i = 0; i < parameters.length; i++) {
				text = text.replace("{"+i+"}", ""+parameters[i]);			
			}
		}

		return text;
	}	
}
